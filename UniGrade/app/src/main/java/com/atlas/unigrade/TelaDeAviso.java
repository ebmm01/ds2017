package com.atlas.unigrade;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

public class TelaDeAviso extends DialogFragment {

    String mensagem = null;
    int id;

    ITelaDeAvisoCallback callback;

    public void setCallback(ITelaDeAvisoCallback callback) {
        this.callback = callback;
    }

    void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public void setMensagem(int conexaofalhou) {
        this.id = conexaofalhou;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {


        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        if(this.mensagem == null){

            builder.setMessage(this.id);
        }else{

            builder.setMessage(this.mensagem);
        }

        builder.setPositiveButton(
                R.string.ok,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        if(callback != null){
                            callback.okPressionado();
                        }

                    }
                }
        );

        //Cria e retorna
        return builder.create();
    }


}