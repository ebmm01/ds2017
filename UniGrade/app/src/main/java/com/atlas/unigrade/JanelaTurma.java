package com.atlas.unigrade;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class JanelaTurma extends AppCompatActivity {


    private static boolean houveMudanca = false;

    Materia materiaBaseDados;
    Materia materiaElencada;

    private AdaptadorListaTurma adaptador;
    private ArrayList<Turma> itemList;

    private ListView listView;

    private String materiaParaLer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_janela_turma);

        listView =(ListView) findViewById(R.id.lista_turmas);

        setWidget();

        Toolbar toolbarturma = (Toolbar) findViewById(R.id.toolbarturma);
        toolbarturma.setTitle("Turmas");
        setSupportActionBar(toolbarturma);

        //getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.drawable.ic_launcher_tpp);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        //linkar spinner
        Spinner spinner_prioridades = (Spinner)findViewById(R.id.spinnerprioridades);
        //adicionar itens ao spinner
        String[] itens = new String[]{"1", "2", "3", "4", "5"};
        //adaptar para a lista do spinner
        ArrayAdapter<String> adaptadorSpinner = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, itens);
        spinner_prioridades.setAdapter(adaptadorSpinner);

        spinner_prioridades.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                Log.v("item", (String) parent.getItemAtPosition(position));

                 //adicionamos a prioridade na matéria elencada
                if(materiaElencada != null){ //só faz sentido dar prioridade se a matéria estiver elencada
                    materiaElencada.setPrioridade(position + 1);

                    //após isso, reescrevemos a matéria
                    Escritor esc = new Escritor(getApplicationContext());
                    esc.escreverMateria(materiaElencada, BaseDeDados.getRaizArquivosElencados());
                }else{

                    //caso a matéria não esteja elencada, vamos guardar a prioridade no obj da basededados
                    //isso não será escrito na base de dados e sim na base de materias elencadas, pois
                    //o objeto da base será copiado para o de elencados

                    materiaBaseDados.setPrioridade(position + 1);
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });

        if(materiaElencada != null) {
            spinner_prioridades.setSelection(materiaElencada.getPrioridade() - 1);
        }
    }

    public void setWidget(){

        itemList = new ArrayList<Turma>();

        materiaParaLer = BaseDeDados.getMateriaParaLer();
        BaseDeDados.setMateriaParaLer(null);

        //primeiro vamos buscar a matéria na base de dados e popular a interface gráfica
        File pastaBaseDados = Leitor.encontrarPastaDaMateria(materiaParaLer, BaseDeDados.getRaizBaseDeDados());
        Leitor leitorBaseDados = new Leitor(pastaBaseDados.getAbsolutePath() + File.separator + "info.txt", getApplicationContext());

        this.materiaBaseDados = leitorBaseDados.lerMateria(true);

        for(Turma t : materiaBaseDados.getTurmas()) {
            itemList.add(t);
        }

        //Definindo título da janela como nome da matéria
        TextView txmaterias = (TextView)findViewById(R.id.tituloturma);
        txmaterias.setText(materiaBaseDados.getNome());
        TextView tvcreditos = (TextView)findViewById(R.id.creditos);
        tvcreditos.setText("Créditos: " + materiaBaseDados.getCreditos());

        //antes de mandar a itemList para o adaptador, vamos ordena-la, colocando as turmas elencadas em primeiro lugar
        itemList = ordenaLista(itemList);

      //Setup Adapter for Listview
        adaptador = new AdaptadorListaTurma(JanelaTurma.this,itemList);

        //e agora verificamos se já existe alguma turma elencada
        File pastaElencada = Leitor.encontrarPastaDaMateria(materiaParaLer, BaseDeDados.getRaizArquivosElencados());

        if(pastaElencada != null) {
            //se a pasta existe, vamos entrar nela e ler a matéria, depois só é preciso comparar as turmas
            Leitor leitorElencada = new Leitor(pastaElencada.getAbsolutePath() + File.separator + "info.txt", getApplicationContext());
            this.materiaElencada = leitorElencada.lerMateria(true);

            for (Turma base : itemList){

                for (Turma elencada : this.materiaElencada.getTurmas()){

                    if(base.getIdentificadorDaTurma().compareTo(elencada.getIdentificadorDaTurma()) == 0){
                        adaptador.setCheckBox(itemList.indexOf(base));
                    }
                }
            }

        }

        listView.setAdapter(adaptador);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView adaptadorView, View view, int position, long l) {
                //Item Selected from list
                adaptador.setCheckBox(position);

                Turma t = (Turma) adaptadorView.getItemAtPosition(position);

                Escritor esc = new Escritor(getApplicationContext());

                if(materiaElencada == null){
                    materiaElencada = new Materia(
                            materiaBaseDados.getNome(),
                            materiaBaseDados.getCodigo(),
                            materiaBaseDados.getCreditos(),
                            materiaBaseDados.getAulasSemanais()
                    );

                    materiaElencada.setPrioridade(materiaBaseDados.getPrioridade());
                }

                if(t.isCheckbox()){
                    //se a turma estiver elencada é pq ela estava não elecanda, então devemos elencar ela
                    materiaElencada.adicionarTurma(t);

                    Toast.makeText(JanelaTurma.this, "Adicionando turma " + t.getIdentificadorDaTurma(),  Toast.LENGTH_SHORT).show();
                }else{
                    //desenlencar a turma
                    materiaElencada.removerTurma(t);

                    Toast.makeText(JanelaTurma.this, "Removendo turma " + t.getIdentificadorDaTurma(),  Toast.LENGTH_SHORT).show();
                }

                esc.escreverMateria(materiaElencada, BaseDeDados.getRaizArquivosElencados());
                houveMudanca = true;

            }
        });

    }

    ArrayList<Turma> ordenaLista(ArrayList<Turma> itemList){

        ArrayList<Turma> listaOrdenada = new ArrayList<>();

        ArrayList<Turma> listaElencadas = new ArrayList<>();
        ArrayList<Turma> listaNaoElencadas = new ArrayList<>();


        File pastaElencada = Leitor.encontrarPastaDaMateria(materiaParaLer, BaseDeDados.getRaizArquivosElencados());

        if(pastaElencada != null) {
            //se a pasta existe, vamos entrar nela e ler a matéria, depois só é preciso comparar as turmas
            Leitor leitorElencada = new Leitor(pastaElencada.getAbsolutePath() + File.separator + "info.txt", getApplicationContext());
            this.materiaElencada = leitorElencada.lerMateria(true);


            //vamos separar as turmas em duas listas
            for (Turma base : itemList) { //turma na base de dados (desejamos saber se ela esta elencada ou nao

                boolean estaElencada = false;

                for (Turma elencada : this.materiaElencada.getTurmas()) {//turmas elencadas

                    //a ideia aqui é comparar a turma com todas que estiverem elencadas

                    //se estiver elencada
                    if (base.getIdentificadorDaTurma().compareTo(elencada.getIdentificadorDaTurma()) == 0) {
                        listaElencadas.add(base);

                        estaElencada = true;

                        break;
                    }
                }

                //se a turma não foi encontrada na materia elencada, adicionamos ela na
                if(estaElencada == false){
                    listaNaoElencadas.add(base);
                }

            }
        }else{

            //se não estiver elencada, então é só adicionar tudo em NãoElencada
            listaNaoElencadas.addAll(itemList);
        }

        Collections.sort(listaElencadas);
        Collections.sort(listaNaoElencadas);

        //agora sabemos quais são elencadas ou não, então adicionamos a lista ordenada na ordem certa
        listaOrdenada.addAll(listaElencadas); //primeiro os elencados
        listaOrdenada.addAll(listaNaoElencadas); //depois os outros


        return listaOrdenada;
    }

    public static boolean houveMudancaNosDados(){

        boolean resposta = houveMudanca;

        if(houveMudanca){
            houveMudanca = false;
        }

        return resposta;
    }
}
