package com.atlas.unigrade;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;


import java.util.ArrayList;
import java.util.List;

public class telaPrincipal extends AppCompatActivity {

    List<String> materiasElencadas;
    private TabLayout tabLayout;
    private ViewPager janelaPrincipal;

    boolean atualizar = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //quando chegarmos aqui, já devemos ter um resultado do atualizador, portanto
        atualizar = Splash.atualizacaoEncontrada;

        //faz o processamento da classe mãe
        super.onCreate(savedInstanceState);

        //inicializar a classe que salva informações sobre os dados do aplicativo
        //é muito importante que inicializar a base de dados seja uma das primeiras
        //coisas que o app faz
        BaseDeDados.inicializarBasesDeDados(getFilesDir(), getApplicationContext());

        //aponta a tela atual para a telaPrincipal
        setContentView(R.layout.activity_tela_principal);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

        //getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.drawable.ic_launcher_tpp);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        janelaPrincipal = (ViewPager) findViewById(R.id.viewpager);

        configJanelaPrincipal(janelaPrincipal);

        tabLayout = (TabLayout) findViewById(R.id.tabs);

        tabLayout.setupWithViewPager(janelaPrincipal);

        //após terminar tudo, se tivermos atualização devemos perguntar ao usuário se ele deseja fazê-la:
        if(atualizar) {
            ConfirmarAtualizacao conf = new ConfirmarAtualizacao();
            conf.callback = this;
            conf.show(getFragmentManager(), "Atualizar");
        }else if(Splash.tempoAcabou){

            TelaDeAviso aviso = new TelaDeAviso();
            aviso.setMensagem(R.string.conexaofalhou);

            aviso.show(getFragmentManager(), "Aviso");
        }else if(Splash.instalacaoIniciada){

            TelaDeAviso aviso = new TelaDeAviso();

            if(Splash.instalacaoCompleta){
                //sucesso

                aviso.setMensagem(R.string.instalacaoSucesso);
                aviso.show(getFragmentManager(), "Instalção");
            }else {

                aviso.setMensagem(R.string.instalacaoFalhou);
                aviso.show(getFragmentManager(), "Instalção");
            }
        }

    }

    void confirmarAtualizacao(boolean resposta){

        if(resposta){
            //o usuário deseja fazer a atualização
            Splash.estaAtualizando = true;

            Intent i = new Intent(telaPrincipal.this, Splash.class);
            startActivity(i);

            finish();
        }
    }


    private void configJanelaPrincipal(ViewPager janelaPrincipal) {

        OneFragment one = new OneFragment();
        TwoFragment two = new TwoFragment();
        ThreeFragment three = new ThreeFragment();

        ViewPagerAdapter adaptador = new ViewPagerAdapter(getSupportFragmentManager());

        adaptador.addFragment(one, "Adicionar matérias");
        adaptador.addFragment(two, "Matérias adicionadas");
        adaptador.addFragment(three, "Grades");

        janelaPrincipal.setAdapter(adaptador);

        two.setContext(getApplicationContext());
        two.setMateriasElencadas(materiasElencadas);

        one.setContext(getApplicationContext());

        three.setContext(getApplicationContext());


        janelaPrincipal.setCurrentItem(1);

        //setContentView(R.layout.fragment_two);


    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_tela_principal, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        //int id = item.getItemId();

        switch (item.getItemId()) {
            case R.id.atualizar:
                ConfirmarAtualizacao confirmarAtualizacao = new ConfirmarAtualizacao();
                confirmarAtualizacao.callback = this;
                confirmarAtualizacao.show(getFragmentManager(), "Atualizar");
                return true;

            case R.id.grades:

                // chamar activity que mostra grades
                janelaPrincipal.setCurrentItem(2, true);
                return true;

            case R.id.sobre:
                Intent i = new Intent(telaPrincipal.this, JanelaSobre.class);
                startActivity(i);
                return true;

            case R.id.addmaterias:

                // chamar activity com matérias para adicionar
                janelaPrincipal.setCurrentItem(0, true);

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // do nothing, just override
    }
}
