package com.atlas.unigrade;

import android.content.Context;
import android.util.Log;;
import java.io.File;
import java.util.Calendar;
import java.util.GregorianCalendar;

/***
 * Classe responsável por baixar o arquivo com a base de dados
 * */
public class Atualizador implements DownloadCallback {
	//Link para o download da base de dados
    private final String zipLink = "https://github.com/guilherme1guy/mwsresult/raw/master/mwsresult.zip";
    private final String txtLink = "https://raw.githubusercontent.com/guilherme1guy/mwsresult/master/info.txt";

    private File arquivoBaixado;

    private Baixador b;
	//Validação da atualização
    private boolean _completo = false;
    private int _progresso = -1;
    private boolean erro = false;
    private boolean timedOut = false;

    private Context _context;

    private boolean mEstaVerificandoPorAtualizacao = false;


    void procurarAtualizacao(){

        mEstaVerificandoPorAtualizacao = true;

        //Criação de um objeto infoTXT na classe File
        File infoTXT = new File(BaseDeDados.getRaizBaseDeDados().getAbsolutePath() + File.separator + "info.txt");

        //se não tivermos o arquivo info.txt é pq a base não existe, portanto precisa ser atualizada
        if(!infoTXT.exists()){
            Splash.atualizacaoEncontrada = true;

            return;
        }


        b = new Baixador
                (
                        _context.getFilesDir() + File.separator + "versaoExterna.txt",
                        _context,
                        this,
                        txtLink
                );

        //inicia o trabalho de download como tarefa async
        b.execute();
    }
	//Aloca o valor do parametro Context ao objeto
    Atualizador(Context c){

        this._context = c;

    }
	//Aloca o valor boolean em timedOut
    void setTempoAcabou(boolean b){
        timedOut = b;

        this.notificarErro(new Exception("Timeout"));

        this.b.cancel(true);
    }

    /***
     * Atualiza o banco de dados do Unigrade com base nos arquivos retirados pelo MWScanner
     * */
    public void baixarAtualizacao(){

        //baixar

        //queremos salvar o zip nos arquivos de cache do android
        //enviamos um referencia a este objeto pois ele será chamado de volta pelo baixador
        b = new Baixador
        (
                _context.getFilesDir() + File.separator + "mws.zip",
                _context,
                this,
                zipLink
        );

        //inicia o trabalho de download como tarefa async
        b.execute();

    }

    public boolean instalarAtualizacao(){

        //primeiro, limpamos todos os dados
        BaseDeDados.apagarTodosOsDados();

        //precisamos atualizar a localização da base de dados antes de descomprimir o arquivo, pois
        //a antiga foi apagada, mas a referencia ainda aponta para ela
        BaseDeDados.inicializarBasesDeDados(_context.getFilesDir(), _context);

        Descompressor d = new Descompressor(this.arquivoBaixado, BaseDeDados.getRaizBaseDeDados());
        boolean sucesso = d.descomprimir();

        //reinicializa a base de dados
        BaseDeDados.inicializarBasesDeDados(this._context.getFilesDir(), _context);

        if(!sucesso){
            //TODO: sinalizar erro para o usuário

            return false;
        }

        return true;
    }

    /***
     * Metodo chamado pelo obj baixador ao completar ao download, com o arquivo baixado como
     * parametro
     * */
    public void downloadCompleto(File f){

        this.arquivoBaixado = f;
        this._completo = true;

        //se estivermos procurando atualizacao, devemos analisar de precisamos de atualizacao
        if(mEstaVerificandoPorAtualizacao){

            Splash.atualizacaoEncontrada = precisaAtualizar();

        }
    }

    private boolean precisaAtualizar(){


        //precisamos comparar o infoTXT com o arquivoBaixado e decidir o resultado
        //true se infoTXT for mais velho que arquivoBaixado
        //false caso contrário

        File infoTXT = new File(BaseDeDados.getRaizBaseDeDados().getAbsolutePath() + File.separator + "info.txt");

        //lemos o arquivo local
        Leitor local = new Leitor(infoTXT, _context);
        local.lerArquivo(true);

        //lemos o arquivo externo
        Leitor externo = new Leitor(arquivoBaixado, _context);
        externo.lerArquivo(true);

        //agora pegamos a primeira linha (é a que contem a data no formato YYYY-MM-DD) e comparamos
        //as duas

        int[] dataLocal = converterStringParaData(local.getLido()[0]);
        int[] dataExterna = converterStringParaData(externo.getLido()[0]);

        //agora vamos comparar as datas usando calendarios

        /*
        * Por algum motivo que vai alem de meu entendimento, Calendars que acabaram de ser criados
        * e que, em tese, tem horários idênticos ainda podem retornar que são diferentes. A solução
        * que aparentemente funciona é dar Calendar.clear() logo após cria-lo para garantir que o
        * Calendar está completamente limpo quando recebe a data. E dessa forma, datas iguais ficam
        * iguais.
        * */

        Calendar calendarioLocal = new GregorianCalendar();
        calendarioLocal.clear();
        calendarioLocal.set(dataLocal[0], dataLocal[1], dataLocal[2]);

        Calendar calendarioExterno = new GregorianCalendar();
        calendarioExterno.clear();
        calendarioExterno.set(dataExterna[0], dataExterna[1], dataExterna[2]);

        //vamos partir da premissa de não precisar atualizar
        boolean resposta = false;

        //caso o calendario externo seja maior devemos atualizar
        if(calendarioExterno.compareTo(calendarioLocal) > 0){
            resposta = true;
        }

        Log.i("Atualizador", "Base de dados e suas versões." +
                "\nLocal: " + local.getLido()[0]  + " (Cal: "+ calendarioLocal.getTime().toString() +")" +
                "\nExterno: " + externo.getLido()[0] + " (Cal: "+ calendarioExterno.getTime().toString() +")" +
                "\nPrecisa atualizar: " + resposta);


        return resposta;
    }

    //retorna no formato:
    //0 - YYYY
    //1 - MM
    //2 = DD
    private int[] converterStringParaData(String s){

        int[] data = new int[3];

        //de 0 até '-'
        data[0] = Integer.parseInt(s.substring(0, s.indexOf('-')));
        //do primeiro '-' até o segundo '-'
        data[1] = Integer.parseInt(s.substring(s.indexOf('-') + 1, s.lastIndexOf('-')));
        //do segundo '-' até o fim
        data[2] = Integer.parseInt(s.substring(s.lastIndexOf('-') + 1, s.length()));

        return data;
    }

    /***
     * status do download deste obj atualizador
     * */
    int statusDownload(){
        return _progresso;
    }

    boolean acabouDownload(){ return _completo; }

    /***
     * Essa função é chamada pelo objeto baixador para poder definir qual o progresso atual do download
     * */
    public void definirProgresso(Integer[] progresso) {

        //como ela **precisa** receber uma array de integers, verificamos qual o maior membro dessa
        //array (e não deve ser aceito um valor anterior ao progresso atual)

        Integer maior = _progresso;

        for (Integer i : progresso) {

            if(i > maior){
                maior = i;
            }
        }

        _progresso = maior;
    }

    /***
     * Metodo chamado pelo obj baixador ao encontrar um erro no download
     * */
    public void notificarErro(Exception e){
        //TODO: exibir erro e cancelar a tarefa

        this._progresso = -1;
        this._completo = true;
        this.erro = true;


    }

    public boolean temErro(){
        return erro;
    }
}
