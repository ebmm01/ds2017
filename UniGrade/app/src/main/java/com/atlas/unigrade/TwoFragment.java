package com.atlas.unigrade;

/**
 * Created by geovana on 22/09/17.
 */
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class TwoFragment extends Fragment {
    List<String> materiasElencadas;
    ListView lista_materiasElencadas;
    private Context mContext;

    public TwoFragment() {

    }

    public void setContext(Context context) {

        this.mContext = context;// necessário para usar getApplicationContext()
    }

    public void setMateriasElencadas(List<String> materiasElencadas){
        this.materiasElencadas = materiasElencadas;
    }

    private List<String> getMateriasElencadas() {

        if(materiasElencadas == null || materiasElencadas.size() < 1 || JanelaTurma.houveMudancaNosDados()) {
            popularListaMaterias();
        }

        return materiasElencadas;
    }

    private void popularListaMaterias(){
        materiasElencadas = new ArrayList<>();

        //depois criamos um novo leitor que aponta para o codMat dos arquivos elencados
        Leitor leitor = new Leitor(BaseDeDados.getRaizArquivosElencados().getAbsolutePath() + File.separator + "matCod.txt", mContext);

        //e pedimos para ele ler o arquivo
        leitor.lerArquivo(true);

        //depois pegamos todas as strings e jogamos na arraylist
        //TODO: otimizar essa parte do código
        if(leitor.getLido() == null || leitor.getLido().length < 1){
            materiasElencadas.add(mContext.getText(R.string.semMateriasElencadas).toString());
        }else {
            for (String s : leitor.getLido()) {

                if(!materiasElencadas.contains(s)){
                    materiasElencadas.add(s);
                }
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_two, container, false);

        lista_materiasElencadas = (ListView) rootView.findViewById(R.id.lista_materiaselencadas);
        ArrayAdapter<String> adaptador = new ArrayAdapter<>(getActivity().getBaseContext(), android.R.layout.simple_list_item_1, getMateriasElencadas());
        lista_materiasElencadas.setAdapter(adaptador);


        lista_materiasElencadas.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {

                String valorDaTabela = arg0.getItemAtPosition(arg2).toString();

                if(valorDaTabela.compareTo(mContext.getText(R.string.semMateriasElencadas).toString()) != 0 && valorDaTabela.compareTo("") != 0){

                    BaseDeDados.setMateriaParaLer(valorDaTabela);

                    Intent in = new Intent(mContext ,JanelaTurma.class);

                    startActivity(in);
                }
            }
        });

        // Inflate the layout for this fragment
        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();

        lista_materiasElencadas = (ListView) getView().findViewById(R.id.lista_materiaselencadas);


        if(lista_materiasElencadas != null) {
            ArrayAdapter<String> adaptador = new ArrayAdapter<>(getActivity().getBaseContext(), android.R.layout.simple_list_item_1, getMateriasElencadas());

            lista_materiasElencadas.setAdapter(adaptador);
        }
    }
}