package com.atlas.unigrade;

/**
 * Created by geovana on 22/09/17.
 */
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.UiThread;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import java.io.File;
import java.text.Normalizer;
import java.util.LinkedList;


public class OneFragment extends Fragment implements IPesquisadorCallback{

    private LinkedList<String> materiasBaseDeDados;

    ListView lista_materiasBaseDados;

    SearchView sv;

    private Context mContext;

    public OneFragment() {

    }

    public void setContext(Context context) {

        this.mContext = context;// necessário para usar getApplicationContext()
    }

    private void popularListaDeMaterias(){

        materiasBaseDeDados = new LinkedList<>();

        //depois criamos um novo leitor que aponta para o codMat dos arquivos elencados
        Leitor leitor = new Leitor(BaseDeDados.getRaizBaseDeDados().getAbsolutePath() + File.separator + "matCod.txt", mContext);

        //e pedimos para ele ler o arquivo
        leitor.lerArquivo(true);

        //depois pegamos todas as strings e jogamos na arraylist
        //TODO: otimizar essa parte do código
        if(leitor.getLido() == null || leitor.getLido().length < 1){
            materiasBaseDeDados.add(getContext().getText(R.string.semBaseDados).toString());
        }else {
            for (String s : leitor.getLido()) {

                if(!materiasBaseDeDados.contains(s)){

                    String materia[] = Materia.interpretaCodMat(s);

                    //dessa forma garantimos o espaço entre o nome da materia e seu codigo
                    String nomeMateria = materia[1] + " - " + materia[0];

                    materiasBaseDeDados.add(nomeMateria);
                }
            }
        }
    }

    private LinkedList<String> getMateriasBaseDeDados(){

        if(materiasBaseDeDados == null || materiasBaseDeDados.size() < 1){
            popularListaDeMaterias();
        }

        return materiasBaseDeDados;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_one, container, false);


        //aqui é o unico lugar onde não devemos usar  atualizarLista(getMateriasBaseDeDados());
        //pois a view ainda não foi desenhada na tela

        lista_materiasBaseDados = (ListView) rootView.findViewById(R.id.lista_materiasBaseDados);
        ArrayAdapter<String> adaptador = new ArrayAdapter<>(getActivity().getBaseContext(), android.R.layout.simple_list_item_1, getMateriasBaseDeDados());

        lista_materiasBaseDados.setAdapter(adaptador);

        lista_materiasBaseDados.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {



                String valorDaTabela = arg0.getItemAtPosition(arg2).toString();

                if(
                        valorDaTabela.compareTo(getString(R.string.semBaseDados)) != 0
                        && valorDaTabela.compareTo(getString(R.string.pesquisando)) != 0
                        && valorDaTabela.compareTo(getString(R.string.semResultadosNaPesquisa)) != 0
                        && valorDaTabela.compareTo("") != 0) {

                    BaseDeDados.setMateriaParaLer(valorDaTabela);

                    Intent in = new Intent(mContext, JanelaTurma.class);

                    startActivity(in);
                }
            }
        });

        sv = (SearchView) rootView.findViewById(R.id.SearchView);

        sv.setFocusable(false);
        sv.setFocusableInTouchMode(false);

        sv.clearFocus();

        sv.setIconified(false);

        sv.setQueryHint("Pesquisar por matérias ou código");

        sv.setOnQueryTextListener(

            new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String s) {

                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {

                    if (newText.length() > 0 && newText.isEmpty() == false) {
                        doSearch(newText);
                    }else{

                        //se o texto for para 0 ou for um espaço, vamos voltar a mostrar tudo
                        atualizarLista(getMateriasBaseDeDados());
                    }

                    return true;
                }
            }
        );



        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onPause() {

        limparFocoDaPesquisa();

        super.onPause();
    }

    @UiThread
    private void limparFocoDaPesquisa(){

        sv = (SearchView) getView().findViewById(R.id.SearchView);

        if(sv != null) {
            sv.clearFocus();
        }
    }

    public void doSearch(String s){

        //primeiro vamos usar apenas caracteres minusculos
        s = s.toLowerCase();

        //não vamos limpar acentuação gráfica e afins, pois confiamos (por enquanto) que o usuário
        //terá a inteligência mínima para...

        //esquece, vamos limpar acentos
        s = limparString(s);

        //agora podemos começar a busca

        //acahar a listView
        lista_materiasBaseDados = (ListView) getView().findViewById(R.id.lista_materiasBaseDados);

        //se acharmos (não é nula)
        if(lista_materiasBaseDados != null){

            //colocamos na UI que estamos pesquiando
            LinkedList<String> textoPesquisa = new LinkedList<>();
            textoPesquisa.add( getContext().getText(R.string.pesquisando).toString());

            atualizarLista(textoPesquisa);

            //então criamos o pesquisador
            Pesquisador.criarPesquisa(getMateriasBaseDeDados(), s, this, getContext());

            //e esperamos pelo callback

        }
    }

    protected static String limparString(String s){

        //primeiro, separamos os acentos das letras, ex: â vira a^
        s = Normalizer.normalize(s, Normalizer.Form.NFD);

        //isso remove tudo que não é belo, recatado e do lar
        s = s.replaceAll("[^\\p{ASCII}]", "");

        return s;
    }

    @Override
    public void onResume() {
        limparFocoDaPesquisa();

        super.onResume();
    }

    @Override @UiThread
    //recebe resultado da pesquisa e coloca na tela
    public void postarResultadoPesquisa(LinkedList<String> resultado) {

        //hack para fazer o resultado chegar no thread da UI
        //precisamos passar coisas final (const) para um runnable, então
        //fazemos esse joguinho com variaveis para fazer funcionar

        LinkedList<String> post;

        if(resultado == null){
            post = getMateriasBaseDeDados();
        }else{
            post = resultado;
        }

        final LinkedList<String> finalPost = post;

        this.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                atualizarLista(finalPost);
            }
        });

    }

    @UiThread
    private void atualizarLista(LinkedList<String> listaParaEscrever){

        View v = getView();
        if(v == null){
            return;
        }

        //achar a listView
        lista_materiasBaseDados = (ListView) v.findViewById(R .id.lista_materiasBaseDados);

        //se acharmos (não é nula)
        if (lista_materiasBaseDados != null) {

            //e recriamos nossa listView
            ArrayAdapter<String> adaptador = new ArrayAdapter<>(getActivity().getBaseContext(), android.R.layout.simple_list_item_1, listaParaEscrever);
            lista_materiasBaseDados.setAdapter(adaptador);

        }
    }


}