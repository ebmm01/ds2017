package com.atlas.unigrade;

import java.text.DecimalFormat;
/***
Classe Horario:

Essa classe representa os horários de uma turma, ela tem conhecimento do dia da semana
e da duração e horário de inicio de uma aula de determinada turma.
*/
public class Horario {
	
	private int dia;
	private int inicio[] = new int[2];
	private int fim[] = new int[2];
	private int duracao;
	
	/*** int Dia, int inicio, int Duração (em horas)*/
	Horario(int dia, int[] inicio, int[] fim){
		//metodo construtor (não é necessário um método destrutor)
		
		this.dia = dia;
		this.inicio = inicio;
		this.fim = fim;
		this.duracao = fim[0] - inicio[0];
	}
	
	public static int[] converterHora(String hora) {
		//Metodo de conversão de uma string hora, para int...
		int horas = stringParaInt(hora.substring(0, hora.indexOf(':')));
		int minutos = stringParaInt(hora.substring(hora.indexOf(':') + 1));
		//Criação de um vetor para armazenar o horario.
		int horario[] = new int[2];
		//Hora na primeira posição, e minutos na segunda posição do vetor.
		horario[0] = horas;
		horario[1] = minutos;

		return horario;
	}
	
	private static int stringParaInt(String s) {


        //remove o que não é numero
        for (char c : s.toCharArray()){

            String testar = "" + c;
	    //Condição para caso o valor ser diferente de número, substituir por vazio.
            if(!"0123456789".contains(testar)){

                s = s.replaceAll(testar, "");
            }
        }

		int valor = 0;
		
		for (int i = 0; i < s.length(); i++) {

				valor += ((s.charAt(i) - 48) * Math.pow(10, s.length()-1-i));
		}
		
		return valor;
	}
	
	
	private int getDia() {
		return this.dia;
	}
	
	String getDia_STR() {
		
		String s;
		//Condicional que transforma o número recebido em "dia", para uma string correspondente.
		switch(this.dia) {
		case 0:
			s = "Dom";
			break;
		case 1:
			s = "Seg";
			break;
		case 2:
			s = "Ter";
			break;
		case 3:
			s = "Qua";
			break;
		case 4:
			s = "Qui";
			break;
		case 5:
			s = "Sex";
			break;
		case 6:
			s = "Sab";
			break;
		default:
			s = "error";
			break;
		}
	
		return s;
	}
	
	/***
	 * Converte um dia (str) para int, no formato utilizado por esta classe. Se retornar -1, houve erro.
	 * */
	public static int converterDia(String strDia) {
		
		strDia = strDia.toLowerCase();
		strDia = strDia.replace('ç', 'c');
		strDia = strDia.replace('á', 'a');
		
		int dia = -1;
		
		if(strDia.contains("dom")) {
			dia = 0;
		}else if (strDia.contains("seg")) {
			dia = 1;
		}else if (strDia.contains("ter")) {
			dia = 2;
		}else if (strDia.contains("qua")) {
			dia = 3;
		}else if (strDia.contains("qui")) {
			dia = 4;
		}else if (strDia.contains("sex")) {
			dia = 5;
		}else if (strDia.contains("sab")) {
			dia = 6;
		}
		
		
		return dia;
	}
	
	private int[] getInicio() {
		return this.inicio;
	}
	
	String getInicio_STR() {
		
		String s;

        DecimalFormat formatter = new DecimalFormat("00");
		
		s = formatter.format(this.inicio[0]) + ":" + formatter.format(this.inicio[1]) + "h";
		
		return s;
	}
	
	String getFim_STR() {
		
		String s;

        DecimalFormat formatter = new DecimalFormat("00");

        s =  formatter.format(this.fim[0]) + ":" + formatter.format(this.fim[1]) + "h";
		
		return s;
	}

	private int[] getFim() {
		return fim;
	}

	boolean temChoque(Horario outro) {
		if(outro.getDia() == this.getDia() ) {
			/*
			&& outro.getDuracao() == this.getDuracao()
					&& outro.getInicio() == this.getInicio()
			*/

			
			//2 casos a verificar para retornar false:
			//começo do outro < meu começo < fim do outro
			//meu começo < começo do outro < meu fim

            //TODO: comparar com minutos

			//se o outro começa depois de mim e antes do meu final
			if(outro.getInicio()[0] > this.getInicio()[0] && outro.getInicio()[0] < this.getFim()[0]){
				return true;
			}

			//se eu começo depois do outro e antes do fim dele
			if(this.getInicio()[0] > outro.getInicio()[0] && this.getInicio()[0] < outro.getFim()[0]){
				return true;
			}

			if(this.getFim()[0] == outro.getFim()[0] && this.getInicio()[0] == outro.getInicio()[0]){
                return true;
            }
            if (this.getInicio()[0] == outro.getInicio()[0]){
				return true;
			}

			return false;
		}
		
		
		return false;
		
	}
}
