package com.atlas.unigrade;

/**
 * Created by geovana on 22/09/17.
 */
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.UiThread;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.LinkedList;


public class ThreeFragment extends Fragment implements IMontadorDeGradeCallback, ITelaDeAvisoCallback{

    Context mContext;

    MontadorDeGrade montadorGrade;

    Button botaoGerador;

    LinkedList<Materia> materiasTurmasSemHorario;

    LinkedList<Button> botoes; //salva os botoes, usamos essa lista para saber qual dos botoes foi usado

    TableLayout tableAguardandoPermissao;

    public ThreeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void setContext(Context context) {

        this.mContext = context;// necessário para usar getApplicationContext()
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_three, container, false);

        botaoGerador = (Button) rootView.findViewById(R.id.montarGrades);

        botaoGerador.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                Toast t = Toast.makeText(getContext(), "Gerando grades...", Toast.LENGTH_SHORT);
                t.show();

                botaoGerador.setEnabled(true);
                botaoGerador.setText("ATUALIZAR GRADES");
                gerarGrades();
            }
        });

        new Handler().postDelayed(new Runnable() {
            /*
             * timer
             */
            @Override
            public void run() {

                atualizarView();
            }
        }, 1000);

        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();


        atualizarView();
    }

    @UiThread
    void atualizarView(){
        View rootView = getView();


        if(rootView == null){
            return;
        }

        final TableLayout tableLayout = (TableLayout) rootView.findViewById(R.id.tabela_grade);//new TableLayout(mContext);

        tableLayout.removeAllViews();

        int tamanho = 0;

        if(montadorGrade != null && montadorGrade.getGradesMontadas() != null){
            tamanho = montadorGrade.getGradesMontadas().size();
        }

        //criamos uma nova lista para salvar a posicao em que uma parte do TableLayout acaba
        botoes = new LinkedList<>(); //usamos os botoes para saber qual deles foi chamado


        // vendo se existem turmas sem horário, só precisamos verificar uma vez, pois se a turma não
        //tiver horários ela estára em todas as grades

        //vou sempre escrever isso na primeira linha das grade, para não ocupar espaço extra na tela
        //antes ele ia crescendo e diminuindo o espaço em que as grades apareciam
        TextView TVsemhorario = new TextView(getContext());
        String escrever = "";

        if(materiasTurmasSemHorario != null){

            //rodando todas as matérias da lista de matérias com turmas sem horário
            for(int msh=0; msh< materiasTurmasSemHorario.size(); msh++){

                //rodando todas as turmas sem horário da materia de index msh
                for(int tsh=0; tsh < materiasTurmasSemHorario.get(msh).getTurmas().size(); tsh++){


                    //pegando nome da matéria e da turma
                    String materia = materiasTurmasSemHorario.get(msh).getNome();
                    String turma = materiasTurmasSemHorario.get(msh).getTurmas().get(tsh).getIdentificadorDaTurma();

                    //adicionando formatação
                    if(escrever.compareTo("") == 0){
                        escrever += "Atenção: ";
                    }

                    //mostrando texto ao usuário
                   escrever += "\nA turma \"" + turma + "\" da matéria \"" + materia + "\" não possui horários cadastrados no matrícula web";
                }

            }
        }

        //colocamos o texto dentro
        TVsemhorario.setText(escrever);

        //e adicionamos a view
        tableLayout.addView(TVsemhorario);

        for(int c=0; c<tamanho; c++){

            TableRow tableRow;
            TextView textView;

            Grade gradeAtual = montadorGrade.getGradesMontadas().get(c);

            for (int i = 0; i < 10; i++) {//linhas
                tableRow = new TableRow(mContext);
                for (int j = 0; j < 7; j++) {//colunas

                    textView = new TextView(mContext);
                    if (i == 0 && j == 0) {
                        textView.setText("GRADE " + (c + 1));// passar para string.xml apos testes
                        textView.setBackgroundColor(Color.parseColor("#ffc299"));
                    } else if (i == 0) {
                        if (j == 1) {
                            textView.setText(" Segunda ");// passar para string.xml apos testes
                        } else if (j == 2) {
                            textView.setText(" Terça ");
                        } else if (j == 3) {
                            textView.setText(" Quarta ");
                        } else if (j == 4) {
                            textView.setText("  Quinta  ");
                        } else if (j == 5) {
                            textView.setText(" Sexta ");
                        } else if (j == 6) {
                            textView.setText(" Sábado ");
                        }
                        textView.setBackgroundColor(Color.parseColor("#ffc299"));
                    } else if( j == 0) {

                        if(i == 1){
                            textView.setText(" 06h/08h ");
                        }else if (i == 2) {
                            textView.setText(" 08h/10h ");// passar para string.xml apos testes
                        } else if (i == 3) {
                            textView.setText(" 10h/12h ");
                        } else if (i == 4) {
                            textView.setText(" 12h/14h ");
                        } else if (i == 5) {
                            textView.setText("  14h/16h  ");
                        } else if (i == 6) {
                            textView.setText(" 16h/18h ");
                        } else if (i == 7) {
                            textView.setText(" 18h/20h ");
                        } else if (i == 8) {
                            textView.setText(" 20h/22h ");
                        }else if (i == 9) {
                            textView.setText(" 22h/24h ");
                        }
                        textView.setBackgroundColor(Color.parseColor("#ffc299"));
                    } else  {

                        //independente do que for, o fundo deve ser branco
                        textView.setBackgroundColor(Color.WHITE);

                        //j == dia
                        //i == hora

                        boolean encontrado = false;

                        for(Materia m : gradeAtual.getMaterias()){

                            int tempoinicio[] = new int[2];
                            tempoinicio[0] = 6 + (i-1)*2;
                            tempoinicio[1] = 0;

                            int tempofim[] = new int[2];
                            tempofim[0] = 6 + (i-1)*2 + 2;
                            tempofim[1] = 0;


                            Horario horarioAtual = new Horario(j, tempoinicio, tempofim);

                            for (Turma t: m.getTurmas()){

                                if(t.temAula(horarioAtual)){

                                    encontrado = true;

                                    textView.setText(m.getNome() + "\n" + t.getIdentificadorDaTurma());

                                    break;
                                }

                            }

                            if(encontrado){
                                break;
                            }


                        }

                        if(!encontrado){
                            textView.setText("--");

                        }

                    }
                    if(i!=0 && j!=0){
                        textView.setMinimumWidth(650);
                    } else if (i!=0){
                        textView.setMinHeight(140);
                    }
                    textView.setPadding(20, 20, 20, 20);
                    textView.setTextColor(Color.parseColor("#000000"));

                    tableRow.setBackgroundColor(Color.WHITE);
                    tableRow.addView(textView);
                }

                tableRow.setBackgroundColor(Color.WHITE);
                tableLayout.addView(tableRow);
            }

            final Button btsalvar = new Button(mContext);

            //salvamos o botao correspondente a essa parte no mesmo indice (em outra lista)
            botoes.add(c, btsalvar);

            btsalvar.setText("Salvar grade");

            btsalvar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    final TableLayout preparado = prepararTabelaFoto(tableLayout, botoes.indexOf(btsalvar));
                    preparado.invalidate(); //redesenhar

                    //vamos rodar depois de um tempo para poder deixar a view ser desenhada
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            salvarImagem(preparado);
                        }
                    }, 500);
                }
            });

            tableLayout.addView(btsalvar);
        }

    }

    @UiThread
    TableLayout prepararTabelaFoto(TableLayout tableLayout, int indexBotao){
        //acredito que as childs de tableLayout sejam as linhas


        /*
         * A ideia aqui é separar em uma nova tableView a grade, como são separadas por um botão,
         * dá para verificarmos essa divisão. Quando chegar em um botão devemos verificar se é o
         * mesmo botão que foi clicado, se não foi devemos resetar a tableLayout que estavamos escrevendo
         * */

        View rootView = getView();

        if(rootView == null){
            return null;
        }

        TableLayout tableDaFoto = (TableLayout) rootView.findViewById(R.id.tabela_desenhar);

        TextView aviso;
        //a primeira posição sempre será um textview com a mensagem de aviso
        //então vamos copiar, pois quero escrever ela na parte de baixo
        aviso = copiarTextView ((TextView) tableLayout.getChildAt(0));

        //começa em 1 para pular a mensagem sem horario
        for(int i = 1; i < tableLayout.getChildCount(); i++){

            //e a linha é uma TableRow, a não ser que seja um botao
            if(tableLayout.getChildAt(i).getClass() == Button.class){

                //é um botão
                Button botao = (Button) tableLayout.getChildAt(i);

                //se for um botao, chegamos ao final de uma grade
                //vamos verificar se é o botao certo

                if(botao == botoes.get(indexBotao)){

                    //se for o certo, podemos parar por aqui (sem adicioná-lo à tableDaFoto)
                    break;


                }else{

                    //se não for, vamos limpar a tableDaFoto
                    tableDaFoto.removeAllViews();

                }

            }else if(tableLayout.getChildAt(i).getClass() == TableRow.class){
                //é uma tableRow

                //não podemos adicionar diretamente, então temos que fazer uma cópia do objeto em um
                //novo objeto
                TableRow linhaAdicionar = new TableRow(getContext());
                TableRow linhaOriginal = ((TableRow) tableLayout.getChildAt(i));

                linhaAdicionar.setMinimumHeight(linhaOriginal.getMinimumHeight());

                //criamos uma cópia de to-do o conteudo
                for(int j = 0; j < linhaOriginal.getChildCount(); j++){

                    //eu espero que todos os filhos da tablerow sejam textviews
                    if(linhaOriginal.getChildAt(j).getClass() == TextView.class) {

                        TextView original = (TextView) linhaOriginal.getChildAt(j);

                        linhaAdicionar.addView(copiarTextView(original));

                    }else{
                        Log.e("SALVAR IMAGEM", "Não é TextView é um: " + tableLayout.getChildAt(i).getClass());
                    }
                }


                //então vamos adicionar
                linhaAdicionar.setBackgroundColor(((ColorDrawable) linhaOriginal.getBackground()).getColor());
                tableDaFoto.addView(linhaAdicionar);

            }else{
                Log.e("SALVAR IMAGEM", "Não é botão, nem table row, é um: " + tableLayout.getChildAt(i).getClass());
            }

        }

        //adiciona os avisos ao final, porem ele precisa de um background para ficar visível
        aviso.setBackgroundColor(Color.WHITE);
        tableDaFoto.addView(aviso);

        return tableDaFoto;
    }

    TextView copiarTextView(TextView original){

        TextView copia = new TextView(getContext());

        copia.setText(original.getText());
        copia.setTextColor(original.getTextColors().getDefaultColor());

        if(original.getBackground() != null) {
            copia.setBackgroundColor(((ColorDrawable) original.getBackground()).getColor());
        }

        copia.setMinimumWidth(original.getMinimumWidth());
        copia.setMinHeight(original.getMinHeight());

        copia.setPadding(original.getPaddingLeft(), original.getPaddingTop(), original.getPaddingRight(), original.getPaddingBottom());

        return copia;
    }

    boolean verificarPermissao(){

        //se a versão do android precisar das permissões explicitas
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {


            //se não tivermos a permissão de escrita
            if(getContext().checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {

                return false;
            }


        }

        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            //temos permissão
            if(tableAguardandoPermissao != null){
                salvarImagem(tableAguardandoPermissao);
            }
        }
    }

    @Override
    public void okPressionado() {
        pedirPermissao();
    }

    void pedirPermissao() {


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            //agora pedimos a permissão
            int req = 0;

            requestPermissions(
                    new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    req
            );
        }
    }

    void salvarImagem(TableLayout tableLayout){


        if(verificarPermissao() == false){

            tableAguardandoPermissao = tableLayout;
            tableAguardandoPermissao = tableLayout;

            //explicar o motivo da permissão
            TelaDeAviso aviso = new TelaDeAviso();

            aviso.setMensagem("Nós precisamos da permissão para poder salvar uma imagem da grade.");
            aviso.setCallback(this);

            aviso.show(getActivity().getFragmentManager(), "Permissão");


            return;
        }


        //Aqui salva somente uma imagem do table layout
        tableLayout.setDrawingCacheEnabled(true);

        //criamos um bitmap que vai armazenar nossa imagem
        Bitmap pageBitmap = Bitmap.createBitmap(tableLayout.getWidth(), tableLayout.getHeight(), Bitmap.Config.ARGB_8888);

        //e um canvas, onde desenharemos o tableLayout (o canvas passa para o bitmap)
        Canvas canvas = new Canvas(pageBitmap);
        tableLayout.draw(canvas);

        //criamos um objeto File que aponta para a imagem da grade

        //objeto que aponta para diretório publico de imagens (com pasta do unigrade)
        final File pastaImagens = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath() + File.separator + "unigrade");
        if(!pastaImagens.exists()){
           Log.i("SALVAR IMAGEM",  "Criar diretório: " + pastaImagens.getAbsolutePath() + (pastaImagens.mkdirs() ? " Sucesso":" Falhou"));
        }

        File file = new File(pastaImagens.getAbsolutePath() + File.separator + "grade" + Calendar.getInstance().getTime().toString() + ".png");

        try {

            //e criamos uma out stream que escreverá na file
            FileOutputStream ostream = new FileOutputStream(file);

            //escrevemos o bitmap do canvas na out stream
            pageBitmap.compress(Bitmap.CompressFormat.PNG, 100, ostream);

            //flush e close, para escrever no arquvio
            ostream.flush();
            ostream.close();

            //redesenhar table view na tela
            //NOTA: aparentemente o metodo ".invalidade()" não pode ser chamado fora do thread da UI
            //isso joga uma exception e faz o processo parar
            //A documentação do android sugere que no lugar de .invalidate() seja usado
            //.postInvalidate(), pois não é um thread de UI. Tanto o postInvalidade quanto o invalidate
            //redesenham a view (no caso tableLayout é uma view)
            //TODO: testar
            tableLayout.postInvalidate();

            //NOTA2: existe a possibilidade de a UI não ser atualizada pois estamos usando o metodo
            //postInvalidate de um thread que não é o de UI, mas como não estamos fazendo mudanças na
            //table view não há problema

            //usamos um thread aqui para poder forçar a execução dessa parte do código na UI
            getActivity().runOnUiThread(new Runnable() {

                @Override
                public void run() {

                    Toast.makeText(getActivity(), "Imagem salva" + pastaImagens.getAbsolutePath(), Toast.LENGTH_SHORT).show();
                }
            });


            tableLayout.setDrawingCacheEnabled(false);
            tableLayout.removeAllViews();

        } catch (Exception e) {

            //usamos um thread aqui para poder forçar a execução dessa parte do código na UI
            getActivity().runOnUiThread(new Runnable() {

                @Override
                public void run() {

                    Toast.makeText(getActivity(), "Houve um erro ao salvar a imagem! Desculpa =(", Toast.LENGTH_SHORT).show();
                }
            });

            e.printStackTrace();
            Log.e("SALVAR IMAGEM", "ERRO");
        }

        //e pedimos para o sistema escanear nossa imagem e atualizar a galeria
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);

        Uri contentUri = Uri.fromFile(file);

        mediaScanIntent.setData(contentUri);
        mContext.sendBroadcast(mediaScanIntent);
    }

    void gerarGrades(){

        //primeiro lemos e montamos os objetos materia da base de elencados

        LinkedList<Materia> materiasElencadas = new LinkedList<>();
        materiasTurmasSemHorario = new LinkedList<>();

        //criamos um novo leitor que aponta para o codMat dos arquivos elencados
        Leitor leitor = new Leitor(BaseDeDados.getRaizArquivosElencados().getAbsolutePath() + File.separator + "codMat.txt", mContext);

        //e pedimos para ele ler o arquivo
        leitor.lerArquivo(true);

        //depois pegamos todas as strings do codMat e lemos suas matérias
        if(leitor.getLido() != null && leitor.getLido().length >= 1){
            //se tiver elencadas
            for (String s : leitor.getLido()) {

                File pastaMateria = Leitor.encontrarPastaDaMateria(s, BaseDeDados.getRaizArquivosElencados());

                Leitor l = new Leitor(pastaMateria.getAbsolutePath() + File.separator + "info.txt", this.mContext);

                Materia lida = l.lerMateria(true);

                //vamos verificar se há uma turma sem horário

                //como vamos alterar as turmas da matéria lida, não podemos fazer um foreach com lida.getTurmas();
                //por isso faremos uma cópia

                LinkedList<Turma> turmas = new LinkedList<>();
                turmas.addAll(lida.getTurmas());

                for(Turma t : turmas){


                    if(t.getAulas() == null){

                        //se não tiver aulas devemos verificar se essa turma já foi adicionada a lista
                        //de matérias com turma se aula

                        boolean encontrada = false;
                        Materia materiaEncontrada = null;

                        for(Materia m : materiasTurmasSemHorario){
                            if(lida.getCodigo().compareTo(m.getCodigo()) == 0){
                                encontrada = true;
                                materiaEncontrada = m;
                                break;
                            }
                        }

                        if(encontrada){
                            //se a matéria já foi separada, vamos adicionar a turma a ela e remover
                            //da turma que vai para o montador

                            lida.removerTurma(t);
                            materiaEncontrada.adicionarTurma(t);
                        }else{

                            //se a matéria não estiver presente na lista de materias e turmas sem
                            //horario, devemos adiciona-la

                            //devemos adicionar somente a turma que não tem horário, para isso, vamos
                            //criar uma cópia da matéria, para não mexer no objeto original
                            Materia copia = new Materia(lida.getNome(), lida.getCodigo(), lida.getCreditos(), lida.getAulasSemanais());
                            //e vamos adicionar apenas a turma sem horário a ela
                            copia.adicionarTurma(t);
                            //e também remover da turma original
                            lida.removerTurma(t);

                            //por fim, adicionar a matéria a lista
                            materiasTurmasSemHorario.add(copia);
                        }

                    }
                }


                //vamos verificar se a materia elencada ficou sem turmas, se esse for
                //o caso, devemos removê-la
                if(lida.getTurmas().size() > 0){
                    materiasElencadas.add(lida);
                }


            }
        }

        //criamos o montador
        montadorGrade = new MontadorDeGrade(materiasElencadas, this);

        //e rodamos ele em outro thread
        Thread t = new Thread(montadorGrade);

        t.run();
    }

    @Override
    public void gradeMontada() {

        Toast t = Toast.makeText(getContext(), "Grades geradas! Confira =)", Toast.LENGTH_SHORT);
        t.show();

        atualizarView();
    }
}