//package com.atlas.unigrade;
//
//
//import java.io.File;
//import java.util.LinkedList;
//import java.util.Scanner;
//
///***
// * essa � uma classe com o exemplo de uso das outras classes
// * */
//public class Prototipo {
//
//    /*
//	public static void main(String[] args) {
//
//		Prototipo t = new Prototipo();
//
//		Leitor.setRaizBaseDados();
//
//		t.menu();
//	}
//	*/
//
//    void menu() {
//
//		while(true) {
//
//			Scanner s = new Scanner(System.in);
//
//			System.out.println("UNIGRADE - PROTOTIPO");
//			System.out.println("O que deseja fazer?");
//			System.out.println("0 - sair");
//			System.out.println("1 - imprimir toda a base de dados");
//			System.out.println("2 - imprimir arquivo cod-mat");
//			System.out.println("3 - imprimir detalhes de uma materia (usando o código)");
//			int resposta = lerResposta(s, 0, 3);
//
//			if(resposta == 0) {
//
//				s.close();
//				return;
//			}else if(resposta == 1) {
//
//				this.testarSistema();
//			}else if (resposta == 2) {
//
//				this.exibirCodMat();
//			}else if(resposta == 3) {
//
//				System.out.println("Digite o código da matéria: ");
//				int cod = lerResposta(s);
//
//				Leitor codMat = lerCodMat();
//
//				File raiz = BaseDeDados.getRaizBaseDeDados().getAbsoluteFile();
//				File pastas[] = raiz.listFiles();
//
//				File pastaMateria = encontraPasta(Integer.toString(cod), pastas);
//
//				Leitor leitorDaMateria = new Leitor(
//						pastaMateria + File.separator + "info.txt"
//					);
//
//				Materia m = leitorDaMateria.lerMateria();
//
//				LinkedList<Turma> turmas = leitorDaMateria.lerTurmaDeMateria();
//				for (Turma turma : turmas) {
//					m.adicionarTurma(turma);
//				}
//
//
//				imprimeMateria(m);
//
//			}
//		}
//	}
//
//	int lerResposta(Scanner s, int valorMinimo, int valorMaximo) {
//
//		int retorno = valorMinimo - 1;
//
//		do {
//
//			retorno = s.nextInt();
//
//			if(retorno < valorMinimo && retorno > valorMaximo) {
//				System.out.println("Opção inválida, tente novamente!");
//				s.nextLine(); //limpa o input
//			}
//
//		} while (retorno < valorMinimo && retorno > valorMaximo);
//
//		return retorno;
//	}
//
//int lerResposta(Scanner s) {
//
//		Integer retorno = s.nextInt();
//
//		while(retorno == null) {
//			System.out.println("Opção inválida, tente novamente!");
//			s.nextLine(); //limpa o input
//
//			retorno = s.nextInt();
//		}
//
//
//		return retorno;
//	}
//
//	void exibirCodMat() {
//
//		Leitor leitor = new Leitor(BaseDeDados.getRaizBaseDeDados().getAbsolutePath() + File.separator + "codMat.txt");
//
//		leitor.lerArquivo();
//
//		for (String s : leitor.getLido()) {
//			System.out.println(s);
//		}
//	}
//
//	/***
//	 * Este metodo utiliza um algoritimo de busca para encontar um arquivo dentro de uma lista de arquivos (usado para encontrar pastas)
//	 * */
//	File encontraPasta(String nomeCompleto, File pastas[]) {
//
//		//bubble
//
//		//retira apenas o codigo da materia, para facilitar a comparacao
//		String codigoMateria = Materia.interpretaCodMat(nomeCompleto)[0];
//		String nomeDaPasta;
//		String codigoDaPasta;
//
//		int limiteInferior = 0;
//		int limiteSuperior = pastas.length;
//		int diferenca;
//		int posicaoTeste;
//
//		while(true) {
//			posicaoTeste = (limiteInferior + limiteSuperior)/2;
//
//
//			nomeDaPasta = pastas[posicaoTeste].getName();
//			//vamos limpar o nome da pasta e ficar só com o código da materia
//			codigoDaPasta = Materia.interpretaCodMat(nomeDaPasta)[0];
//
//			diferenca = codigoMateria.compareToIgnoreCase(codigoDaPasta);
//
//			if(
//					limiteSuperior == limiteInferior
//					|| diferenca == 0
//					|| codigoDaPasta.contains(codigoMateria)
//			){
//
//					return pastas[posicaoTeste];
//
//
//			} else if(diferenca > 0) {
//
//				limiteInferior = posicaoTeste;
//
//			} else{ //if(diferenca < 0)
//
//				limiteSuperior = posicaoTeste;
//
//			}
//		}
//	}
//
//	Leitor lerCodMat() {
//
//		//vamos ler o arquivo codMat
//		Leitor leitor = new Leitor(BaseDeDados.getRaizBaseDeDados().getAbsolutePath() + File.separator + "codMat.txt");
//
//		//isso faz o objeto leitor ler o arquivo inteiro
//		leitor.lerArquivo();
//
//		return leitor;
//
//	}
//
//	void testarSistema() {
//
//		/*
//		 * a ideia � a seguinte:
//		 * (vamos supor que todos os dados que eu passei sejam lidos dos dados)
//		 *	1 - ler a materia do banco de dados e criar um objeto com ela
//		 * 	2 - ler as turmas e horaios
//		 *  3 - preparar os objetos horarios de cada turma
//		 *  4 - criar objeto turma com os horarios
//		 *  5 - adicionar turma a materia
//		 * */
//
//		//NOTA: isso vai ser muito pesado, nai ha otimizacao e estamos lendo tudo de uma vez
//		//e nao de acordo com a demanda. Eu sei, isso e algo muito burro de se fazer. Mas este
//		//e um prototipo.
//
//		LinkedList<Materia> materias = new LinkedList<Materia>();
//
//		//vamos ler o arquivo codMat
//		Leitor leitor = lerCodMat();
//
//		File raiz = BaseDeDados.getRaizBaseDeDados().getAbsoluteFile();
//		File pastas[] = raiz.listFiles();
//
//		//le completamente o conteudo do codMat (enquanto o que leu n�o foi null)
//		for (String linha : leitor.getLido()) {
//
//			//cria um Leitor apontado para o arquivo de informacoes da materia
//
//
//			Leitor leitorDaMateria = new Leitor(
//						encontraPasta(linha, pastas) + File.separator + "info.txt"
//					);
//
//			//le os dados da materia e a adiciona a lista
//			//TODO: verificar se n�o saiu algo null daqui
//			Materia m = leitorDaMateria.lerMateria();
//			materias.add(m);
//
//			//as altera��es no objeto 'm' continuam a valer, mesmo com ele na lista
//
//			//agora vamos ler as turmas da materia
//			LinkedList<Turma> turmasLidas = leitorDaMateria.lerTurmaDeMateria();
//			for (Turma turma : turmasLidas) {
//				//e adicionar todas a materia
//				m.adicionarTurma(turma);
//			}
//
//			imprimeMateria(m);
//		}
//
//		return;
//	}
//
//	void imprimeMateria(Materia mat) {
//		//exibe tudo que tem no objeto
//		System.out.println("-----------------");
//		System.out.println("Materia: ");
//		System.out.println("Código: " + mat.getCodigo());
//		System.out.println("  Nome: " + mat.getNome());
//		System.out.println("  Aulas semanais: " + mat.getAulasSemanais());
//		System.out.println("  Creditos: " + mat.getCreditos());
//		System.out.println("  Turmas de " + mat.getNome());
//
//		for (Turma turma : mat.getTurmas()) {
//			System.out.println("    Turma " + turma.getIdentificadorDaTurma());
//			System.out.println("    Professor: " + turma.getProfessor());
//
//			System.out.println("    Horarios: ");
//			for (Horario aula : turma.getAulas()) {
//				System.out.println("      " + aula.getDia_STR() + ": " + aula.getInicio_STR() + " - " + aula.getFim_STR());
//			}
//		}
//		/*Output:
//		-------
//		Materia:
//
//		Nome: Calculo I
//		Creditos: 6
//		Turmas de Calculo I
//		  Turma AA
//		  Professor: LINDOMAR
//		  Horarios:
//		    seg: 16:00h - 18:00h
//		    qua: 16:00h - 18:00h
//		    sex: 16:00h - 18:00h
//		*/
//	}
//}
//
//
