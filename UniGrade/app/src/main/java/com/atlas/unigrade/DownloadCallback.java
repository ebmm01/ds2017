package com.atlas.unigrade;

import java.io.File;

public interface DownloadCallback {

    void downloadCompleto(File f);
    void definirProgresso(Integer[] progresso);
    void notificarErro(Exception e);

}
