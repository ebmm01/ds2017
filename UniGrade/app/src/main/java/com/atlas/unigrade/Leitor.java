package com.atlas.unigrade;


import android.content.Context;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Scanner;


/***
Classe responsavel pela retirada de informações fornecidas pelo matriculaWeb.
*/
public class Leitor{

	private File caminhoArquivo = null;
	private Scanner scanner;

	private Context context;

	private BufferedReader buffer;
	
	private String[] lido;
	

	Leitor(String caminho, Context c){
        this.context = c;

        this.caminhoArquivo = new File(caminho);
        this.scanner = new Scanner(System.in);
    }

    Leitor(File caminho, Context c){
        this.context = c;

        this.caminhoArquivo = caminho;
        this.scanner = new Scanner(System.in);

    }

	/***
	 * Este metodo tenta definir um novo arquivo dentro deste objeto, retorna true se
	 * tudo ocorreu bem e false caso constr�rio
	 * */
	private boolean definirArquivo(String caminho){
		
		//caso o arquivo apontado pelo caminho j� tenha sido aberto, ele devera ser
		//fechado antes de trocarmos o arquivo
		if(this.buffer != null){
			
			try{
				this.buffer.close();
				this.buffer = null;
				
				this.caminhoArquivo = new File(caminho);	
		
				return true;
			}catch (IOException e) {
				Log.e("LEITOR", "Erro na abertura do arquivo: \n" + e.getMessage());
	    		
	    		return false;
		
			}
		}else{
			this.caminhoArquivo = new File(caminho);	
			
			return true;
		}
	}
	
	
	/***
	 * Essa funcao le o arquivo inteiro que está definido neste objeto, retorna false se
	 * o caminho nao estiver definido ou se não foi possível ler o arquivo
	 * */
	boolean lerArquivo(){
		if(this.caminhoArquivo == null){
			Log.i("LEITOR", "Arquivo não foi lido, pois é NULL");
			return false;
		}
		
		try {
			
			//So e necessario criar o buffer quando o arquivo mudar, o buffer se mantem 
			//na ultima posição lida do arquivo
			if(this.buffer == null){
				//tenta criar o objeto buffer dentro deste objeto
				this.criarBuffer();
			}

			//cria uma lista para salvar to-do o conteudo que foi lido
            LinkedList<String> lido = new LinkedList<>();
            String s;

            do {
                //le linha por linha e adiciona a lista
                s = this.buffer.readLine();

                if(s != null){
                    lido.add(s);
                }

                //ler até o que foi lido ser null, ou seja, fim do arquivo
            }while (s != null);

            //converte a linkedlist para string[]
            this.lido = new String[lido.size()];

            for (int i = 0; i < this.lido.length; i++){
                this.lido[i] = lido.get(i);
            }

			Log.i("LEITOR", "Lido: " + this.getLido());

            return true;
 		}
		catch (Exception e) {
            //se tiver erro, cai aqui
			Log.e("LEITOR", "Erro na abertura do arquivo: " + e.getMessage());
    		
    		return false;
		}
	}

    boolean lerArquivo(boolean ignorarLinhasVazias){
        boolean retorno = this.lerArquivo();

        if(ignorarLinhasVazias && this.lido != null) {

			Log.i("LEITOR", "Limpando linhas vazias");

            String[] lidoNormal = this.lido;

            LinkedList<String> novoLido = new LinkedList<>();

            for (int i = 0; i < lidoNormal.length; i++) {

                if(lidoNormal[i] != null && lidoNormal[i].compareTo("") != 0 && lidoNormal.length > 0){

                    novoLido.add(lidoNormal[i]);
                }
            }

            if(novoLido.size() < 1){
                this.lido = null;
            }else{

                this.lido = new String[novoLido.size()];

                for (int i = 0; i < novoLido.size(); i++){
                    this.lido[i] = novoLido.get(i);
                }
            }
        }


        return retorno;
    }
	
	public String[] getLido() {
		return lido;
	}

	/***
     * Este metodo cria o buffer do arquivo e joga os erros para quem estiver chamando resolver
     * */
	private void criarBuffer() throws FileNotFoundException, UnsupportedEncodingException {

		Log.i("LEITOR", "Criando buffer para " + this.caminhoArquivo.getAbsolutePath());

        //havia um bug com os caracteres em portugues com o java. Ele queria ler em ANSII,
        // mas precisamos do UTF-8 por causa da acentuacao grafica, então definimos o
        //buffer para o modo UTF-8
        this.buffer = new BufferedReader(
                new InputStreamReader( new FileInputStream(this.caminhoArquivo), "UTF-8")
        );
	}
	
	/***
	 * Se este for um objeto que aponta para uma matériam tentamos montar a materia e retornar ela
	 * Retorna null se algo deu errado (ex: não é uma matéria)
	 * */
	private Materia lerMateria() {
			
		if(!this.caminhoArquivo.toString().contains("info.txt")) {
			return null;
		}

		Log.i("LEITOR", "Lendo matéria: " + this.caminhoArquivo.getAbsolutePath());

		this.lerArquivo();
	
		//primeira linha representa o codigo da materia
		String codMateria = this.getLido()[0];
		
		//a segunda o nome
		String nomeMateria = this.getLido()[1];
		
		//a terceira os creditos
		String creditosMateria = this.getLido()[2];
		
		//e a ultima as aulas semanais (lidas em string, precisa converter depois
		String aulasSemanais = this.getLido()[3];

        int prioridade = 1;

        if (this.getLido().length > 3) {
            try {

               prioridade = Integer.parseInt(this.getLido()[4]);
            }catch (Exception e){

                e.printStackTrace();
            }
        }
		
		int aulas;
		try {
			aulas = (int)Math.floor(Double.parseDouble(aulasSemanais));
		} catch (NumberFormatException e) {
				e.printStackTrace();
			
			aulas = 0;
		}
		
		//cria a materia e retorna
        Materia m = new Materia(nomeMateria, codMateria, creditosMateria, aulas);
        m.setPrioridade(prioridade);

		return m;
	}


	public Materia lerMateria(boolean lerTurmas) {

        if(!lerTurmas ){
            return lerMateria();
        }else{

            Materia materia = lerMateria();

            LinkedList<Turma> turmas = lerTurmaDeMateria();

            for(Turma t : turmas){
                materia.adicionarTurma(t);
            }

            return materia;
        }
	}
	
	/***
	 * Le os dados de uma turma de um arquivo (letra).txt
	 * */
	private Turma lerTurma() {

		Log.i("LEITOR", "Lendo turma: " + this.caminhoArquivo.getAbsolutePath());
		
		this.lerArquivo();

		Horario horarios[] = this.lerHorariosDaTurma();

		//retira o nome da turma a partir do nome do arquivo
		String letraDaTurma = this.caminhoArquivo.getName().substring(0, this.caminhoArquivo.getName().indexOf('.')); 
		
		String professor = this.lerProfessorDaTurma();

        String campus = this.lerCampusDaTurma();

		return new Turma(letraDaTurma, professor, horarios, campus);
		
	}
	
	/***
	 * Este metodo le as turmas de uma materia, funciona quando este objeto Leitor esta apontado para
	 * um "info.txt"
	 * */
	private LinkedList<Turma> lerTurmaDeMateria() {
		
		LinkedList<Turma> retorno = new LinkedList<>();

		//ler as turmas, devemos abrir a pasta da materia apontada por este obj e 
		//listar todos os arquivos dentro dele (menos info.txt)
		File pasta = new File(this.caminhoArquivo.getParent());
		
		Leitor leitorDaTurma = null;

		if(pasta.listFiles() == null){
			return retorno;
		}
		
		//e para cada arquivo
		for (File f : pasta.listFiles()) {
		
			//verificar se queremos mexer nele
			if(!f.getName().contains("info") && !f.isDirectory()) {
				
				//e se preparar para ler a turma, criando um Leitor que aponta para
				//um arquivo de turma
				if(leitorDaTurma == null) {
					leitorDaTurma = new Leitor(f.getAbsolutePath(), this.context);
				}else {
					leitorDaTurma.definirArquivo(f.getAbsolutePath());
				}
				
				//e depois adicionando a turma a nossa lista
				retorno.add(leitorDaTurma.lerTurma());
			}
		}
		
		return retorno;	
	}
	
	/***
	 * Extrai os horários de um arquivo de horarios
	 * */
	private Horario[] lerHorariosDaTurma() {


		//caso não tenha nenhum horário marcado, devemos retornar null
		//ou seja, se a primeira linha não for um dia da semana, não existem horários
		int dia = Horario.converterDia(this.getLido()[0]);

		if(dia == -1){
			return null;
		}

		//a quantidade de aulas é o tamanho do arqivo da turma, menos a linha do professor, dividido por 3 (cada aula leva 3 linhas)
		int qtdAulas = (this.getLido().length - 1)/3;
		Horario aulas[] = new Horario[qtdAulas];
		
		for (int i = 0; i < qtdAulas; i++) {
		
			Horario h = new Horario(
					Horario.converterDia(this.getLido()[i*3]), 
					Horario.converterHora(this.getLido()[i*3 + 1]), 
					Horario.converterHora(this.getLido()[i*3 + 2])
				);
			
			aulas[i] = h;
	
			
		}
		
		return aulas;
		
	}
	
	/***
	 * Este metodo le o professor da turma do arquivo apontado por este objeto.
	 * */
	private String lerProfessorDaTurma() {			

		//o professor está na penultima linha do arquivo
		return this.getLido()[this.getLido().length - 2];
	}

	private String lerCampusDaTurma(){

		//o campus é a ultima linha do arquivo
		return this.getLido()[this.getLido().length -1];
	}


	@Nullable
	static File encontrarPastaDaMateria(String nomeCompleto, File raizDaBusca){

		Log.i("LEITOR", "Procurando " + nomeCompleto +  "em " + raizDaBusca.getAbsolutePath());

		LinkedList<File> pastas = new LinkedList<>();

		Collections.addAll(
		        pastas,
                raizDaBusca.listFiles(new FileFilter() {
                    @Override
                    public boolean accept(File pathname) {

                        if(pathname.isDirectory()){
                            return true;
                        }

                        return false;
                    }
                })
        );

		Collections.sort(pastas);

		//retira apenas o codigo da materia, para facilitar a comparacao
		String codigoMateria = Materia.interpretaCodMat(nomeCompleto)[0];
		codigoMateria = codigoMateria.replaceAll(" ", "");

        if(codigoMateria == null || pastas == null || pastas.size() < 1){
            return null;
        }

		String nomeDaPasta;
		String codigoDaPasta;

		int limiteInferior = 0;
		int limiteSuperior = pastas.size() - 1;
		int diferenca;
		int posicaoTeste;

		while(true) {
            if(limiteSuperior < limiteInferior){
                return null;
            }

			posicaoTeste = (int) Math.floor( (limiteInferior + limiteSuperior)/2 );


			nomeDaPasta = pastas.get(posicaoTeste).getName();

			//vamos limpar o nome da pasta e ficar só com o código da materia
			codigoDaPasta = Materia.interpretaCodMat(nomeDaPasta)[0];
            codigoDaPasta = codigoDaPasta.replaceAll(" ", "");

            if(codigoDaPasta == null){
                posicaoTeste -= 1;
                continue;
            }

			diferenca = codigoMateria.compareToIgnoreCase(codigoDaPasta);

			if(
					limiteSuperior == limiteInferior
					|| diferenca == 0
			){

					if(codigoDaPasta.compareToIgnoreCase(codigoMateria) == 0){

                        //só vamos retornar, caso tenham o mesmo codigo
                       return pastas.get(posicaoTeste);
                    }else{
                        return null;
                    }


			} else if(diferenca > 0) {

				limiteInferior = posicaoTeste + 1;

			} else{ //if(diferenca < 0)

				limiteSuperior = posicaoTeste - 1;
			}
		}
	}
}


