package com.atlas.unigrade;

import java.util.LinkedList;

public interface IPesquisadorCallback {

    void postarResultadoPesquisa(LinkedList<String> resultado);
}
