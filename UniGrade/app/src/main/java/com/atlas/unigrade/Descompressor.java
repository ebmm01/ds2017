package com.atlas.unigrade;

import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/***
 * O código dessa classe foi feito baseado em tutoriais online. Comentei ao máximo para aumentar o
 * conhecimento sobre o código.
 * */
public class Descompressor {

    private File _arquivoZip;
    private File localFinal;


    private InputStream _streamArqiovoZip;

    //tag para debug
    static final String TAG = "Descompressor";


    public Descompressor(File arquivoZip, File localParaDescompactar) {
        _arquivoZip = arquivoZip;
        this.localFinal = localParaDescompactar;

        _checarDiretorio(this.localFinal.getAbsolutePath());
    }



    public boolean descomprimir() {
        try  {
            Log.i(TAG, "Começando a descomprimir");

            //se o objeto ja tiver a strim, vamos usa-la (fica null caso não tenha)
            InputStream fileInputStream = _streamArqiovoZip;

            //se a inputStream não existir, cria-se uma para ler o arquivo zip
            if(fileInputStream == null) {
                fileInputStream = new FileInputStream(_arquivoZip);
            }

            //e transformamos o input de arquivo em um input de zip
            ZipInputStream zin = new ZipInputStream(fileInputStream);

            //o zip possui 'entradas' (entry) para cada arqivo, criamos uma referencia para poder
            //mexer com elas mais a frente
            ZipEntry ze;

            //para cada 'entrada" dos dados de zin (enquanto houver, ao final será null e então
            //saimos do loop
            while ((ze = zin.getNextEntry()) != null) {

                Log.v(TAG, "Descomprimindo " + ze.getName());

                //verifica se a entrada é uma pasta, se for descomprime como pasta
                if(ze.isDirectory()) {

                    //ou seja, verifica o diretorio, o que tambem cria a pasta, se ela não existir
                    _checarDiretorio(this.localFinal.getAbsolutePath() + "/" + ze.getName());
                } else {

                    //caso seja um arquivo, vamos ler de dentro do zip e extrair para o arquivo
                    //criamos uma FileOutputStream para escrever no arquivo
                    FileOutputStream fout = new FileOutputStream(new File(this.localFinal, ze.getName()));

                    //e vamos escrever por bytes, pois não sabemos o conteúdo exato de dentro do zip
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    byte[] buffer = new byte[8*1024];

                    int count;

                    // ler e escrever do zip
                    //essa linha do while possui umas peculiaridades
                    //zin.read(buffer) lê uma parte do arquivo zip para o nosso buffer
                    //ele retorna a quantidade de bits que foram lidos, ou -1 caso não existam mais
                    //bits para ler (End of File)
                    while((count = zin.read(buffer)) != -1)
                    {

                        //no escrevemos todos os bits do buffer na nossa output stream
                        //ou seja, tiramos do input e mandamos para output
                        baos.write(buffer, 0, count);

                        //transformamos em uma array de bytes
                        byte[] bytes = baos.toByteArray();

                        //e escrevemos ela no arquivo
                        fout.write(bytes);

                        //reseta o output buffer, ou seja, descarta tudo que há nele
                        baos.reset();
                    }

                    //fecha tudo
                    fout.close();
                    zin.closeEntry();
                }

            }

            //fecha o arquivo zip
            zin.close();

            //retorna true para sinalizar que deu tudo certo
            return true;

        } catch(Exception e) {
            Log.e(TAG, e.toString());

            return false;
        }

    }

    //verifica se a string dada é uma pasta e cria ela se necessário
    private void _checarDiretorio(String dir) {
        File f = new File(dir);

        if(dir.length() >= 0 && f.isDirectory() == false ) {
            f.mkdirs();
        }
    }


}
