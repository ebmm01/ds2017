package com.atlas.unigrade;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

/**
 * Created by Guilherme on 23/09/2017.
*/

public class ConfirmarAtualizacao extends DialogFragment {

    telaPrincipal callback;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.confirmarAtualizacao)

                .setPositiveButton(R.string.atualizar, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        callback.confirmarAtualizacao(true);
                    }
                })

                .setNegativeButton(R.string.cancelar, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        callback.confirmarAtualizacao(false);
                    }
                });

        //Cria e retorna
        return builder.create();
    }
}
