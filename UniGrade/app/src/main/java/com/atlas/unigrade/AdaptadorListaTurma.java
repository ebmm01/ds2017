package com.atlas.unigrade;

/**
 * Created by geovana on 27/09/17.
 */

import android.view.LayoutInflater;
import java.util.ArrayList;
import android.view.View;
import android.view.ViewGroup;
import android.content.Context;
import android.widget.BaseAdapter;
import android.widget.*;


public class AdaptadorListaTurma extends BaseAdapter{

    private Context activity;
    private ArrayList<Turma> data;
    private static LayoutInflater inflater = null;
    private View vi;
    private ViewHolder viewHolder;

    public AdaptadorListaTurma(Context context, ArrayList<Turma> items) {
        this.activity = context;
        this.data = items;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Turma getItem(int i) {


        return data.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        vi = view;
        //Populate the Listview

        Turma items = data.get(position);

        if(view == null) {
            vi = inflater.inflate(R.layout.lista_turma, null);
            viewHolder = new ViewHolder();
            viewHolder.checkBox = (CheckBox) vi.findViewById(R.id.checkbox);
            viewHolder.identificador = (TextView) vi.findViewById(R.id.identificador);
            viewHolder.professor = (TextView) vi.findViewById(R.id.professor);
            viewHolder.aulas = (TextView) vi.findViewById(R.id.aulas);

            viewHolder.campus = (TextView) vi.findViewById(R.id.campus);
            vi.setTag(viewHolder);
        }

        viewHolder = (ViewHolder) vi.getTag();
        viewHolder.identificador.setText(items.getIdentificadorDaTurma());
        viewHolder.professor.setText(items.getProfessor());
        viewHolder.campus.setText(items.getCampus());

        //Formato: DIA\nHH:MM - HH-MM\nDIA2...
        String str_aulas = "";

        Horario[] aulas = data.get(position).getAulas();

        if (aulas == null) {
            str_aulas = "Não há horários registrados no Matricula Web para esta turma";

        } else {
            //lê todas as aulas e monta os dados em uma string conforme o formato descrito
            for (Horario h : aulas) {
                str_aulas += h.getDia_STR() + " " + h.getInicio_STR() + " - " + h.getFim_STR() + "\n";
            }


        }

        viewHolder.aulas.setText(str_aulas);

        if(items.isCheckbox()){
            viewHolder.checkBox.setChecked(true);
        }
        else {
            viewHolder.checkBox.setChecked(false);
        }
        return vi;
    }

    public void setCheckBox(int position){
        //Update status of checkbox
        Turma items = data.get(position);
        items.setCheckbox(!items.isCheckbox());
        notifyDataSetChanged();
    }

    private class ViewHolder{
        TextView identificador;
        TextView professor;
        TextView aulas;
        TextView campus;
        CheckBox checkBox;
    }
}