<?php
function deleteFolder($path){
	if (is_dir($path) === true){
        	$files = array_diff(scandir($path), array('.', '..'));
        	foreach ($files as $file){
            		deleteFolder(realpath($path) . DIRECTORY_SEPARATOR . $file);
        	}
		return rmdir($path);
	}
	else if (is_file($path) === true){
		return unlink($path);
	}
	return false;
}

function get_links($link, $pattern){
	//return array
	$ret = array();
        $dom = new domDocument;

     	@$dom->loadHTML(file_get_contents($link));

     	$dom->preserveWhiteSpace = false;

      	@$links = $dom->getElementsByTagName('a');

    	foreach ($links as $tag) {

			//echo $tag->getAttribute('href');
			if(strpos($tag->getAttribute('href'),$pattern) !== false){
				$ret["https://matriculaweb.unb.br/graduacao/" . $tag->getAttribute('href')] = $tag->childNodes->item(0)->nodeValue;
   			}
		}
   	return $ret;
}

function filtrarDia($dia){

	if(strpos($dia, "Segunda")!== false){
		return "Segunda";
	}

    if(strpos($dia, "Terça")!== false){
        return "Terça";
    }

    if(strpos($dia, "Quarta")!== false){
        return "Quarta";
    }

    if(strpos($dia, "Quinta")!== false){
        return "Quinta";
    }

    if(strpos($dia, "Sexta")!== false){
        return "Sexta";
    }

    if(strpos($dia, "Sábado")!== false){
        return "Sábado";
    }

    if(strpos($dia, "Domingo")!== false){
        return "Domingo";
    }

	return "-";
}

function filtrarHorario($horario){

	$diaFiltrado = filtrarDia(substr($horario, strpos($horario, "</td><td>")-10, 10));
	$indexPrimeiroHorarioFiltrado = strpos($horario, "</td><td>") + strlen("</td><td>");
	$indexSegundoHorarioFiltrado = strpos($horario, "</td><td>", $indexPrimeiroHorarioFiltrado) + strlen("</td><td>");
	$primeiroHorario = substr($horario, $indexPrimeiroHorarioFiltrado, 5);
	$segundoHorario = substr($horario, $indexSegundoHorarioFiltrado, 5);

	return $diaFiltrado . "\n" . $primeiroHorario . "\n" . $segundoHorario . "\n";
}

function filtrarNome($nome){
	for($aux = 0; $aux < strlen($nome); $aux++){
		if($nome[$aux] == "<"){
			return substr($nome, 0, $aux);
		}
	}
}
function validaHorario($horario){
	if(strlen($horario) >=17 && strlen($horario) <= 21 && strpos($horario, ":") !== false){
		return 1;
	}
	return 0;
}

function filtraNomeDisciplina($nome){

	$nome = str_replace('/', '', $nome);

	$nome = str_replace('á', 'a', $nome);
	$nome = str_replace('Á', 'A', $nome);
	$nome = str_replace('à', 'a', $nome);
	$nome = str_replace('À', 'A', $nome);
	$nome = str_replace('â', 'a', $nome);
	$nome = str_replace('Â', 'A', $nome);
	$nome = str_replace('ã', 'a', $nome);
	$nome = str_replace('Ã', 'A', $nome);

	$nome = str_replace('é', 'e', $nome);
	$nome = str_replace('É', 'E', $nome);
	$nome = str_replace('ê', 'e', $nome);
	$nome = str_replace('Ê', 'E', $nome);
	
	$nome = str_replace('Í', 'I', $nome);
	$nome = str_replace('í', 'i', $nome);
	$nome = str_replace('Î', 'I', $nome);
	$nome = str_replace('î', 'i', $nome);
	
	$nome = str_replace('Ô', 'O', $nome);
	$nome = str_replace('ô', 'o', $nome);
	$nome = str_replace('ó', 'o', $nome);
	$nome = str_replace('Ó', 'O', $nome);	
	$nome = str_replace('õ', 'o', $nome);
	$nome = str_replace('Õ', 'O', $nome);
	

	$nome = str_replace('Û', 'U', $nome);
	$nome = str_replace('û', 'u', $nome);
	$nome = str_replace('Ú', 'U', $nome);
	$nome = str_replace('ú', 'u', $nome);
	
	$nome = str_replace('ç', 'c', $nome);
	$nome = str_replace('Ç', 'C', $nome);

    $nome = str_replace('-', ' ', $nome);


	return $nome;
}

function removeTagsFromString($string, $materiaPath, $disciplineFullName){
	global $basePath;
	$indexCodigoDisciplina = strpos($string, "Código da Disciplina") + 30;
	$codigoDisciplina = substr($string, $indexCodigoDisciplina, 6);

	$indexNomeDisciplina = strpos($string, "Nome");
	$tamNomeDisciplina = strpos($string, "<i class='fa fa-external-link'></i></a></td></tr><tr><th>Créditos")-$indexNomeDisciplina-114;
	$nomeDisciplina = substr($string, $indexNomeDisciplina+114, $tamNomeDisciplina);
	$nomeDisciplina = filtraNomeDisciplina($nomeDisciplina);
	$disciplineFullName = filtraNomeDisciplina($disciplineFullName);
	$indexQtdCreditos = strpos($string, "(Teor-Prat-Ext-Est)") + 36;
	$qtdCreditos = substr($string, $indexQtdCreditos, 15);
	$indexCampus = strpos($string, "<div class='panel-body'><h4>");
	$nomeCampus = filtrarNome(substr($string, $indexCampus+28, 100));
	$handle = fopen($materiaPath . DIRECTORY_SEPARATOR ."codMat.txt", "a+");
	fwrite($handle, $codigoDisciplina . "-" . $disciplineFullName . "\n");
	fclose($handle);

	$handle = fopen($materiaPath . DIRECTORY_SEPARATOR . "matCod.txt", "a+");
	fwrite($handle, $disciplineFullName . "-" . $codigoDisciplina . "\n");
    fclose($handle);

    @mkdir ($materiaPath . $codigoDisciplina . "-" . $nomeDisciplina);

	//armazena os dados gerais da matéria
	$handle = fopen($materiaPath . $codigoDisciplina . '-' . $nomeDisciplina . DIRECTORY_SEPARATOR . "info.txt", "w+");

    fwrite($handle, $codigoDisciplina . "\n");
	fwrite($handle, $nomeDisciplina . "\n");
	fwrite($handle, $qtdCreditos . "\n");

	$teor = substr($qtdCreditos, 0, 3);
	$prat = substr($qtdCreditos, 4, 3);
	$ext = substr($qtdCreditos, 8, 3);

	$nroAulasSemana = (intval($teor) + intval($prat) + intval($ext)) / 2;

	fwrite($handle, $nroAulasSemana . "\n");
	fwrite($handle, $c);
	fclose($handle);

	$lastPos = 0;
	while ( ($lastPos = strpos($string, "<td class='turma'>", $lastPos)) !== false) {
    		$positions[] = $lastPos;
    		$lastPos = $lastPos + strlen("<td class='turma'>");
	}

	$turmas = array();
	$qtdTurmas = count($positions);

	for($value = 0; $value < $qtdTurmas; $value++){

		if($value != $qtdTurmas-1){

			$turmas[$value] = substr($string, $positions[$value], $positions[$value+1] - $positions[$value]);
		}else{

			$turmas[$value] = substr($string, $positions[$value]);
		}
	}

	for($value = 0; $value < $qtdTurmas; $value++){

		$turmaAtual = $turmas[$value];
		$codigoTurma = substr($turmaAtual, 18, 2);

		if($codigoTurma[1] == '<') {
            $codigoTurma[1] = ' ';
        }

		$handle = fopen($materiaPath . $codigoDisciplina . '-' . $nomeDisciplina . DIRECTORY_SEPARATOR ."$codigoTurma.txt", "w+");

		$lastPos = 0;
		$posHorarios = array();
		$count = 0;

		while(($lastPos = strpos($turmaAtual, "<table class='table table-striped table-bordered table-condensed'>", $lastPos))!== false) {
			$posHorarios[$count++] = $lastPos;
			$lastPos = $lastPos + strlen("<table class='table table-striped table-bordered table-condensed'>");
		}
		foreach($posHorarios as $posHAtual){
			$horario = substr($turmaAtual, $posHAtual, strpos($turmaAtual, "</td></tr><tr><td>", $posHAtual) - $posHAtual);
			$newHorario = filtrarHorario($horario);
			if(validaHorario($newHorario)){
				fwrite($handle, $newHorario);
			}
			else{
				$h2 = fopen($basePath . "deumerda.txt", "w+"); //só para debug -> apgar isso
				fwrite($h2, $nomeDisciplina . "\n");
				fwrite($handle, "XXX\n\n\n");
			}
		}
		if(strpos($turmaAtual, "<table>A Designar") !== false){
			fwrite($handle, "A Designar\n");
		}
		else{
			$posProfessor = strpos($turmaAtual, "<td><table><tr><td>") + strlen("<td><table><tr><td>");
			fwrite($handle, filtrarNome(substr($turmaAtual, $posProfessor)) . "\n");
		}
		fwrite($handle, $nomeCampus . "\n");	
		fclose($handle);

		$posProfessor = null;
		$horario = null;
		$posHorarios = null;
	}
}

function getDisciplinesClasses($disciplineLink, $disciplinePath, $disciplineFullName){

    $data = file_get_contents($disciplineLink);
	$pattern_short = '{<div\s+class="body"\s*>((?:(?:(?!<div[^>]*>|</div>).)++|<div[^>]*>(?1)</div>)*)</div>}si';
	$matchcount = preg_match_all($pattern_short, $data, $matches);

	removeTagsFromString($matches[1][0], $disciplinePath, $disciplineFullName);
}

function getDepartamentDiscipline($departamentLink, $departamentPath){
	$urls = get_links($departamentLink, "oferta_dados.aspx?");

	if(sizeof($urls) > 0){

			$cont = 0;
			$len = sizeof($urls);

		foreach($urls as $key=>$value){
			echo "analisando Disciplina(" . ($cont + 1) . " / " . $len . "): $value\n";

			$cont = $cont + 1;

			getDisciplinesClasses($key, $departamentPath, $value);
       		}	
	}
}	

function getCampusDepartament($campus){

	$path = realpath(dirname(__FILE__)) . DIRECTORY_SEPARATOR . "MWScanner" . DIRECTORY_SEPARATOR . "result" . DIRECTORY_SEPARATOR;

	switch($campus){
		case 0:
			$urlToGet = "https://matriculaweb.unb.br/graduacao/oferta_dep.aspx?cod=1";
			break;
		case 1:
			$urlToGet = "https://matriculaweb.unb.br/graduacao/oferta_dep.aspx?cod=2";
			break;
		case 2:
	       	$urlToGet = "https://matriculaweb.unb.br/graduacao/oferta_dep.aspx?cod=3";
			break;
	    case 3:
			$urlToGet = "https://matriculaweb.unb.br/graduacao/oferta_dep.aspx?cod=4";
			break;
	}

	$urls = get_links($urlToGet, "oferta_dis.aspx?");

	if(sizeof($urls) > 0){

		$cont = 0;
		$len = sizeof($urls);

		foreach($urls as $key=>$value){
			echo "analisando Departamento (" . ($cont + 1) . " / " . $len . "): $value\n";

			$cont = $cont + 1;

			getDepartamentDiscipline($key, $path);
    	}
	}
}

function setDataInfo($path){

	if(file_exists($path . "info.txt")){
		unlink($path . "info.txt");
	}

	$handle = fopen($path . "info.txt", "w+");

	fwrite($handle, date('Y-m-d'));

	fclose($handle);

	return;
}

ini_set("log_errors", 1);
$myfile = fopen(realpath(dirname(__FILE__)) . DIRECTORY_SEPARATOR . "php-error.log", "w+");
fclose($myfile);
ini_set("error_log", realpath(dirname(__FILE__)) . DIRECTORY_SEPARATOR . "php-error.log");

$basePath = realpath(dirname(__FILE__)) . DIRECTORY_SEPARATOR . "MWScanner". DIRECTORY_SEPARATOR . "result" . DIRECTORY_SEPARATOR;

deleteFolder(realpath(dirname(__FILE__)) . DIRECTORY_SEPARATOR . "MWScanner");

mkdir(realpath(dirname(__FILE__)) . DIRECTORY_SEPARATOR . "MWScanner", 0777);
mkdir(realpath(dirname(__FILE__)) . DIRECTORY_SEPARATOR . "MWScanner" . DIRECTORY_SEPARATOR . "result" . DIRECTORY_SEPARATOR, 0777);

setDataInfo($basePath);

getCampusDepartament(0);
getCampusDepartament(1);
getCampusDepartament(2);
getCampusDepartament(3);


/*organiza os arquivos em ordem alfabética*/

$array = explode("\n", file_get_contents($basePath . 'codMat.txt'));
$array = array_unique($array);
sort($array);

$handle = fopen($basePath . 'codMat.txt', "w+");
foreach($array as $value){
    fwrite($handle, $value.PHP_EOL);
}
fclose($handle);

$array = explode("\n", file_get_contents($basePath . 'matCod.txt'));
$array = array_unique($array);
sort($array);

$handle = fopen($basePath . 'matCod.txt', "w+");
foreach($array as $value){
    fwrite($handle, $value.PHP_EOL);
}
fclose($handle);

?>
